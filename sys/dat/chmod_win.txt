
/.htaccess:666
/forum/files/:777

/files/:777
/files/avatars/:777
/files/avatars_list/:777
/files/foto/:777
/foto/:777

/style/themes/:777

/sys/obmen/files/:777
/sys/obmen/screens/:777

/sys/ini/:777

/sys/cache/:777
/sys/cache/users/:777
/sys/cache/other/:777
/sys/cache/images/:777

/sys/tmp/:777

/sys/sql_update/:777
/sys/update_fiera/:777
/sys/update_user/:777


/sys/modules/files/:777
/sys/modules/log/:777
/sys/modules/backup/:777
/sys/modules/tmp/:777

/sys/dat/:777
/sys/dat/default.ini:666
/sys/dat/if_password.txt:666
/sys/dat/if_reg.txt:666
/sys/dat/if_user_link_dir.txt:666
/sys/dat/link_dir_err.txt:666
/sys/dat/if_shell_dir.txt:666
/sys/dat/chmod.txt:666
/sys/dat/chmod_win.txt:666

/sys/dat/settings.conf:666
/sys/dat/shif.conf:666